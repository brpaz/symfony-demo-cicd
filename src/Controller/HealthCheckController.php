<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller that provides healthcheck endpoints.
 */
class HealthCheckController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Check the health of the application.
     *
     * @Route("/_health", methods={"GET"}, name="healthcheck")
     */
    public function health()
    {
        $statusCode = 200;
        $data = [
            'status' => 'OK',
        ];

        $dbConnetion = $this->em->getConnection()->ping();

        if (!$dbConnetion) {
            $statusCode = 500;
            $data['doctrine'] = 'Failed to conenct to database';
        }

        return new JsonResponse($data, $statusCode);
    }
}
